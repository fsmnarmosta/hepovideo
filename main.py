from flask import Flask, request, render_template
from random import randint

import time

app = Flask(__name__)

sessions = {}

@app.route("/", methods=['GET'])
def index():
    return render_template("index.html")

@app.route("/master", methods=['POST'])
def new_session():
    i = str(randint(1, 10**9))
    sessions[i] = {"url": request.form["url"], "video_time": 0, "real_time":time.time(), "playing":False}
    return render_template("master.html", video_url=sessions[i]["url"], slave_url=f"/watch/{i}", session_id=i)

@app.route("/watch/<url>", methods=['GET'])
def watch(url):
    return render_template("slave.html", video_url=sessions[url]["url"], session_id=url)

@app.route("/update/<url>", methods=['GET'])
def update(url):
    sessions[url]["video_time"] = float(request.args["video_time"])
    sessions[url]["real_time"] = float(request.args["real_time"]) / 1000
    sessions[url]["playing"] = request.args["playing"] == "true"
    return "OK"

@app.route("/get/<url>", methods=['GET'])
def get(url):
    return '{"video_time":' + str(sessions[url]["video_time"]) + ', "real_time":' + str(sessions[url]["real_time"]) + ', "playing":' + str(sessions[url]["playing"]).lower() + '}'
