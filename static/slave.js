const video = document.getElementById('video');
const id = document.getElementById('session_id').value;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function sync_video() {
    var xmlHttp = new XMLHttpRequest();
    var last_updated = new Date(0);
    while(true){
        xmlHttp.open( "GET", "../get/"+id, true );
        xmlHttp.onreadystatechange = function() { 
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
                var data = JSON.parse(xmlHttp.responseText);
                var real_time = new Date(parseFloat(data.real_time) * 1000);
                var video_time = parseFloat(data.video_time);
                if(real_time > last_updated){
                    console.log(real_time - last_updated);
                    if(data.playing){
                        video.currentTime = video_time + (real_time - Date.now()) / 1000;
                        video.play();
                    }else{
                        video.currentTime = video_time;
                        video.pause();
                    }
                    last_updated = real_time;
                }
            }
        }
        xmlHttp.send( null );
        await sleep(1000);
    }
}

sync_video();
