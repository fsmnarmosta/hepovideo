const video = document.getElementById('video');
const id = document.getElementById('session_id').value;

function sendEvent(playing){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "update/"+id+"?video_time="+video.currentTime+"&real_time="+Date.now()+"&playing="+playing, true );
    xmlHttp.send( null );
}

video.onseeked = (event) => {
    sendEvent(!video.paused)
};

video.onpause = (event) => {
    sendEvent(false);
};

video.onplay = (event) => {
    sendEvent(true);
};
